# Yaydoo Test

## Run the project

1. Install all the project dependencies, run the following command:
```
npm install
```
2. Run the project with the following command:
```
npm run start
```

## Dependencies
| Packages         | Description                                                                 |
|------------------|-----------------------------------------------------------------------------|
| express          | Node Framework to create a server app.                                      |
| express-validator| Is a package to validate data.                                              |
| helmet           | Setting HTTP headers to provide secure to the app                           |
| cors             | Enabled cors to connect to the api                                          |
| dotenv           | Managed environment variables                                               |
| jsonwebtoken     | Create a token for authentication                                           |
| md5              | For encrypt the passwords                                                   |
| moment           | To format dates                                                             |
| mocha            | For testing                                                                 |
| chai             | It's using with mocha for the test                                          |

## Times
| Task                    | Time                 |
|-------------------------|----------------------|
| Create repository       | 10 min               | 
| Create project structure| 20 min               |
| Login                   | 40 min               |
| Create users            | 30 min               |
| Delete users            | 15 min               |
| Users list              | 40 min               |
| Transactions            | 50 min               |
| Tests                   | 15 min               |

## API DOCUMENTATION

1. LOGIN
**Path:** /api/auth
**Method:** POST
**Body Params (required):**
```
email = adminuser@gmail.com
password = s3cr3tpass
```
**Body response:**
```
{
    "error": false,
    "message": "Sign In successfully!",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlciI6IlN1cGVyIiwiZW1haWwiOiJhZG1pbnVzZXJAZ21haWwuY29tIiwiaWF0IjoxNjI3ODUwMTYwLCJleHAiOjE2MjkwNTk3NjB9.ES6ucGoNmjoEDVl0xIw8owuXDkWD_vReAM5sqQJ9rmc"
}
```
**Body response error:**
```
{
    "error": true,
    "message": "Email or password are incorrect!"
}
```

2. CREATE USER
**Path:** /api/users
**Method:** POST
**Body Params (required):**
```
{
  name = the name of the user
  last_name = the last name of the user
  email =  the user email
  password = password for login
  account_number = number for the account 
}
```
**Body response:**
```
{
    "error": false,
    "message": "User created successfully!",
    "user": {
        "name": "Super",
        "last_name": "Administrador",
        "email": "adminuser@gmail.com",
        "password": "fe4b2b3d666e0d4da4aa8eb7dd39efb2",
        "account": {
            "number": 45812742353,
            "balance": 0,
            "transactions": []
        },
        "createdAt": "01/08/2021 15:00:07",
        "updatedAt": "01/08/2021 15:00:07",
        "id": 1
    }
}
```
**Body response error:**
```
{
    "error": true,
    "message": "Some errors was ocurred!",
    "errors": {
        "errors": [
            {
                "value": "scrtpass",
                "msg": "Must contain a number",
                "param": "password",
                "location": "body"
            },
            {
                "msg": "The account number is required",
                "param": "account_number",
                "location": "body"
            }
        ]
    }
}
```

3. DELETE USER
**Path:** /api/users/:id
**Method:** DELETE
**Query Params (required):**
```
id: the user id to delete
```
**Body response:**
```
{
    "error": false,
    "message": "User deleted successfully!",
    "user": {
        "name": "Super",
        "last_name": "Administrador",
        "email": "adminusesr@gmail.com",
        "password": "ba5cd7c3362d498b6b96394b6ed2d826",
        "account": {
            "number": 32423435,
            "balance": 0,
            "transactions": []
        },
        "createdAt": "01/08/2021 17:23:25",
        "updatedAt": "01/08/2021 17:23:25",
        "id": 2
    }
}
```

4. USERS LIST
**Path:** /api/users?date=
**Method:** GET
**Query Params (required):**
```
date = filter users by date
```
**Body response:**
```
{
    "error": false,
    "message": "Users list!",
    "users": [
        {
            "name": "Super",
            "last_name": "Administrador",
            "email": "adminuser@gmail.com",
            "password": "fe4b2b3d666e0d4da4aa8eb7dd39efb2",
            "account": {
                "number": 45812742353,
                "balance": 0
            },
            "createdAt": "01/08/2021 15:00:07",
            "updatedAt": "01/08/2021 15:00:07",
            "id": 1
        }
    ]
}
```

5. CREATE TRANSACTION
**Path:** /api/transactions
**Method:** POST
**Body Params (required):**
```
  userId = the user id who do the transaction
  transactionType = withdrawal or deposit
  amount = amount of the transaction
  accountNumber = account number
```
**Body response:**
```
{
    "error": false,
    "message": "Transaction complete!",
    "transaction": {
        "number": 45812742353,
        "balance": 200,
        "transactions": [
            {
                "transactionType": "deposit",
                "amount": 200,
                "createdAt": "01/08/2021 17:32:29",
                "updatedAt": "01/08/2021 17:32:29"
            }
        ]
    }
}
```
**Body response error:**
```
{
    "error": true,
    "message": "The transaction don't be completed!"
}
```

6. TRANSACTIONS LIST
**Path:** /api/transactions?date
**Method:** POST
**Body Params (required):**
```
date = filter transactions by date
```
**Body response:**
```
{
    "error": false,
    "message": "Transactions list!",
    "transactions": [
        {
            "account_number": 1232095365,
            "account_balance": 400,
            "transactions": [
                {
                    "transactionType": "withdrawal",
                    "amount": -200,
                    "createdAt": "02/08/2021 13:57:55",
                    "updatedAt": "01/08/2021 13:57:55"
                },
                {
                    "transactionType": "withdrawal",
                    "amount": -200,
                    "createdAt": "01/08/2021 14:07:14",
                    "updatedAt": "01/08/2021 14:07:14"
                }
            ],
            "user": {
                "name": "Alejandro",
                "last_name": "Salgado",
                "email": "mymail@gmail.com"
            }
        },
        {
            "account_number": 1232445365,
            "account_balance": 400,
            "transactions": [
                {
                    "transactionType": "withdrawal",
                    "amount": -200,
                    "createdAt": "01/08/2021 13:57:55",
                    "updatedAt": "01/08/2021 13:57:55"
                },
                {
                    "transactionType": "withdrawal",
                    "amount": -200,
                    "createdAt": "01/08/2021 14:07:14",
                    "updatedAt": "01/08/2021 14:07:14"
                }
            ],
            "user": {
                "name": "Alejandro",
                "last_name": "Salgado",
                "email": "mymail1@gmail.com"
            }
        }
    ]
}
```

7. TRANSACTIONS LIST BY USER
**Path:** /api/transactions/:userId?date
**Method:** POST
**Query Params (required):**
```
userId = the user id for search transactions
date = filter
```
**Body response:**
```
{
    "error": false,
    "message": "Transactins list!",
    "transactions": {
        "user": {
            "name": "Super",
            "last_name": "Administrador",
            "email": "adminuser@gmail.com"
        },
        "transactions": [
          {
                    "transactionType": "withdrawal",
                    "amount": -200,
                    "createdAt": "02/08/2021 13:57:55",
                    "updatedAt": "01/08/2021 13:57:55"
                },
                {
                    "transactionType": "withdrawal",
                    "amount": -200,
                    "createdAt": "01/08/2021 14:07:14",
                    "updatedAt": "01/08/2021 14:07:14"
                }
        ]
    }
}
```