// import packages
const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const dotenv = require('dotenv').config();

// import resources
const routes = require('./routes');

const app = express();

// app middlewares
app.use( helmet() );
app.use( cors() );
app.use( express.json() );
app.use( express.urlencoded({ extended: false }) );

// app routes
app.use( '/', routes );

// server start
const port = process.env.APP_PORT;
app.listen( port, () => console.log(`Server listen on: http://localhost:${port}`) );