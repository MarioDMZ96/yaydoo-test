const { check } = require('express-validator');

const transactionDataValidator = (method) => {
  switch (method) {
    case 'create-transaction': return createTransactionValidator();
  }
}

const createTransactionValidator = () => {
  return [
    check('userId').exists().withMessage('The user id is required')
    .isNumeric().withMessage('The id must be a number'),
    check('transactionType').exists().withMessage('The transaction type is required'),
    check('amount').exists().withMessage('The amount is required').isFloat().withMessage('The amount must be a number'),
    check('accountNumber').exists().withMessage('The account number is required')
  ];
}

module.exports = transactionDataValidator;