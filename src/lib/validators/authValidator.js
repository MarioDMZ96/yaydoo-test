// import packages
const { check } = require('express-validator');

const authDataValidator = (method) => {
  switch (method){
    case 'sign-in': return loginValidator();
  }
}

const loginValidator = () => {
  return [
    check('email').exists().withMessage('The user email is required').isEmail().withMessage('The email is incorrect'),
    check('password').exists().withMessage('Password is required')
    .isLength({ min: 8 }).withMessage('Must be at least 8 chars long')
    .matches(/\d/).withMessage('Must contain a number'),
   ];
}

module.exports = authDataValidator;