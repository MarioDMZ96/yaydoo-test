// import packages
const { check } = require('express-validator');
const { get } = require('../../models/user');

const userDataValidator = (method) => {
  switch (method){
    case 'create-user': return createUserValidator();
  }
}

const createUserValidator = () => {
  return [
    check('name').exists().withMessage('The user name is required'),
    check('last_name').exists().withMessage('The user last name is required'),
    check('email').exists().withMessage('The user email is required').isEmail().withMessage('The email is incorrect')
    .custom( async (value) => {
      const users = await get();
      let index = users.findIndex( user => user.email === value);
      if (index >= 0 ){
        return Promise.reject('e-mail already in use');
      }
    }),
    check('password').exists().withMessage('Password is required')
    .isLength({ min: 8 }).withMessage('Must be at least 8 chars long')
    .matches(/\d/).withMessage('Must contain a number'),
    check('account_number').exists().withMessage('The account number is required')
  ];
}

module.exports = userDataValidator;