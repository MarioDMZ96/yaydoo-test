const dotenv = require('dotenv').config();
const port = process.env.APP_PORT;
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

chai.use(chaiHttp);
const API_URL = `http://localhost:${port}`;
const API_ENDPOINT = '/api/auth';

describe('LOGIN', () => {

  it('return a jwt', done => {
    chai.request(API_URL)
    .post(API_ENDPOINT)
    .send({ email: "adminuser@gmail.com", password: "s3cr3tpass" })
    .end( (err, resp ) => {
      expect(resp).to.have.status(200);
      expect(resp).to.be.an('object');
      expect(resp.body).to.have.property('token');
      done();
    })
  })

});