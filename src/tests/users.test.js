const dotenv = require('dotenv').config();
const port = process.env.APP_PORT;
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const jwt = process.env.JWT_TEST;

chai.use(chaiHttp);
const API_URL = `http://localhost:${port}`;
const API_ENDPOINT = '/api/users';

describe('USERS', () => {

  it('return a users list', done => {
    chai.request(API_URL)
    .get(API_ENDPOINT)
    .set('Authorization', jwt)
    .end( (err, resp ) => {
      expect(resp).to.have.status(200);
      expect(resp).to.be.an('object');
      expect(resp.body.users[0]).to.have.property('name');
      expect(resp.body.users[0]).to.have.property('account');
      done();
    })
  })

});