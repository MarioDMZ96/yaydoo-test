const jwt = require('jsonwebtoken');
const dotenv = require('dotenv').config();

const jwtSecret = process.env.JWT_SECRET;

module.exports = authValidate = (req, res, next) => {
  const token = req.get('Authorization');
  jwt.verify(`${token}`,jwtSecret, (error,decode) => {
    if (error){
      return res.status(401).json({error: true,message: 'Access Denied!'});
    }
    req.user_id = decode.id;
    next();
  });
}