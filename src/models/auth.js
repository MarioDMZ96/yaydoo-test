const { select, insert } = require('../services/queries');

const login = async (filePath, data) => {
  let users = await select(filePath);
  let index = users.findIndex( user => 
    user.email === data.email && 
    user.password === data.password
    );
  if ( index >= 0){
    return users[index];
  } else {
    return null;
  }
}

module.exports = {
  login
}