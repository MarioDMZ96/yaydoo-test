const { select, insert } = require('../services/queries');
const dotenv = require('dotenv').config();
const usersDb = process.env.DB_FILE;

const save = async (userId, number, transaction) => {
  let data = await select(usersDb);
  let index = data.findIndex( item => item.id == userId && item.account.number == number);
  if ( index >= 0 ){
    if ( (data[index].account.balance + transaction.amount) < 0 ) return {error: true, message: 'The transaction don\'t be completed!'}
    data[index].account.transactions.push(transaction);
    data[index].account.balance = data[index].account.balance + transaction.amount;
    await insert(usersDb, data);
    return {error: false, message: 'Transaction complete!', transaction: data[index].account}
  } else {
    return {error: true, message: 'The user doesn\'t exists!'};
  }
}

const get = async () => {
  return data = await select(usersDb);
}

module.exports = {
  save,
  get
}