const { select, insert } = require('../services/queries');
const dotenv = require('dotenv').config();
const usersDb = process.env.DB_FILE;

const save = async (user) => {
  let users = await select(usersDb);
  user.id = users.length === 0 ? 1 : users[users.length -1].id + 1;
  users.push(user);
  return result = await insert(usersDb,users);
}

const remove = async (userId) => {
  let users = await select(usersDb);
  if ( users.length === 1 ) return { error: true, message: 'The Database can\'t be empty!'}
  let index = users.findIndex( user => user.id == userId);
  if (index >= 0) {
    let user = users[index];
    users.splice(index,1);
    await insert(usersDb,users);
    return {error: false, message: 'User deleted successfully!', user};
  }
}

const get = async () => {
  return users = await select(usersDb);
}

module.exports = {
  save,
  remove,
  get
}