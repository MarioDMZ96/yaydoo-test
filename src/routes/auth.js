// import packages
const { Router } = require('express');
const { signIn } = require('../controllers/authController');
const authDataValidator = require('../lib/validators/authValidator');

const router = Router();

router.post( '/auth', authDataValidator('sign-in'), signIn );

module.exports = router;