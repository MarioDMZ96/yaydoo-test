// import packages
const { Router } = require('express');
const { createUser, deleteUser, getUsers } = require('../controllers/usersController');
const authValidate = require('../middlewares/authValidator');
const userDataValidator = require('../lib/validators/userValidator');

const router = Router();

router.post( '/users', authValidate, userDataValidator('create-user'), createUser );
router.delete( '/users/:id', authValidate, deleteUser );
router.get( '/users', authValidate, getUsers );

module.exports = router;