// import packages
const express = require('express');
const dotenv = require('dotenv').config();

const app = express();
const prefix = process.env.API_PREFIX;

const authRoutes = require('./auth');
const usersRoutes = require('./users');
const transactionsRoutes = require('./transactions');

app.get( '/', (req, res) => res.status(200).send('Welcome to the API!!') );
app.use( `/${prefix}`, usersRoutes );
app.use( `/${prefix}`, authRoutes );
app.use( `/${prefix}`, transactionsRoutes);

module.exports = app;