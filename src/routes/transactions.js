const { Router } = require('express');
const { createTransaction, getTransactions, getTransactionByUser } = require('../controllers/transactionsController');
const authValidate = require('../middlewares/authValidator');
const transactionDataValidator = require('../lib/validators/transactionValidator');

const router = Router();

router.post( '/transactions', authValidate, transactionDataValidator('create-transaction'), createTransaction);
router.get( '/transactions', authValidate, getTransactions );
router.get( '/transactions/:userId', authValidate, getTransactionByUser );

module.exports = router;