const moment = require('moment');
// import model functions
const { validationResult } = require('express-validator');
const { save, get } = require('../models/transaction');

const createTransaction = async (req, res) => {
  try {
    const { userId, transactionType, amount, accountNumber } = req.body;
    const errors = validationResult(req);
    if ( !errors.isEmpty() ){
      return res.status(400).json({error: true, message: 'Some errors was ocurred!', errors});
    }
    const newAmount = transactionType === 'withdrawal' ? -amount : amount;
    const transaction = {
      transactionType,
      amount: newAmount,
      createdAt: moment().format('DD/MM/YYYY HH:mm:ss'),
      updatedAt: moment().format('DD/MM/YYYY HH:mm:ss')
    };
    const result = await save(userId, accountNumber, transaction);
    return res.status(200).json(result);
  } catch (error) {
    return res.status(500).json({error: true, message: 'An error was ocurred!'});
  }
}

const getTransactions = async (req, res) => {
  try {
    const dateFilter = req.query.date;
    const result = await get();
    let data = [];
    data = result.map( item => {
      return {
        account_number: item.account.number,
        account_balance: item.account.balance,
        transactions: item.account.transactions,
        user: {
          name: item.name,
          last_name: item.last_name,
          email: item.email
        }
      }
    });
    if (dateFilter){
      for ( let index = 0; index < data.length; index += 1){
        data[index].transactions = data[index].transactions.filter( item => item.createdAt.split(' ')[0] === dateFilter);
      }
    } 
    return res.status(200).json({error: false, message: 'Transactions list!', transactions: data});
  } catch (error) {
    return res.status(500).json({error: true, message: 'An error was ocurred!'});
  }
}

const getTransactionByUser = async (req, res) => {
  try {
    const userId = req.params.userId;
    const dateFilter = req.query.date;
    const result = await get();
    let index = result.findIndex( item => item.id == userId);
    let data = {
      user: {name: result[index].name, last_name: result[index].last_name, email: result[index].email},
      transactions: result[index].account.transactions
    };
    if (dateFilter) data.transactions = data.transactions.filter( item => item.createdAt.split(' ')[0] === dateFilter );
    return res.status(200).json({error: false, message: 'Transactins list!', transactions: data});
  } catch (error) {
    return res.status(500).json({error: true, message: 'An error was ocurred!'});
  }
}

module.exports = {
  createTransaction,
  getTransactions,
  getTransactionByUser
}
