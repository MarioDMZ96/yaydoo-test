const moment = require('moment');
const { save, remove, get } = require('../models/user');
const { validationResult } = require('express-validator');
const encrypt = require('../services/encrypt');

const createUser = async (req, res) => {
  try {
    const { name, last_name, email, password , account_number } = req.body;
    const errors = validationResult(req);
    if ( !errors.isEmpty() ){
      return res.status(400).json({error: true, message: 'Some errors was ocurred!', errors});
    }
    const user = {
      name,
      last_name,
      email,
      password: encrypt(password),
      account: {
        number: account_number,
        balance: 0,
        transactions: []
      },
      createdAt: moment().format('DD/MM/YYYY HH:mm:ss'),
      updatedAt: moment().format('DD/MM/YYYY HH:mm:ss')
    }
    await save(user);
    return res.status(200).json({error: false, message: 'User created successfully!', user});
  } catch (error) {
    return res.status(500).json({error: true, message: 'An error was ocurred!'});
  }
}

const deleteUser = async (req, res) => {
  try {
    const userId = req.params.id;
    const result = await remove(userId);
    return res.status(200).json(result);
  } catch (error) {
    return res.status(500).json({error: true, message: 'An error was ocurred!'});
  }
}

const getUsers = async (req, res) => {
  try {
    const dateFilter = req.query.date;
    const result = await get();
    let users = [];
    if (dateFilter){
      users = result.filter( item => item.createdAt.split(' ')[0] === dateFilter );
    } else {
      users = result;
    }
    users.map( item => {
      delete item.account.transactions;
      return item
    });
    return res.status(200).json({error: false, message: 'Users list!', users});
  } catch (error) {
    return res.status(500).json({error: true, message: 'An error was ocurred!'});
  }
}

module.exports = {
  createUser,
  deleteUser,
  getUsers
}