const { login } = require('../models/auth');
const { validationResult } = require('express-validator');
const dotenv = require('dotenv').config();
const usersDb = process.env.DB_FILE;
const encrypt = require('../services/encrypt');
const jwtGenerate = require('../services/jwt');

const signIn = async (req, res) => {
  try {
    const { email, password } = req.body;
    const errors = validationResult(req);
    if ( !errors.isEmpty() ){
      return res.status(400).json({error: true, message: 'Some errors was ocurred!', errors});
    }
    const user = {
      email,
      password: encrypt(password)
    };
    const userDb = await login(usersDb, user);
    if ( !userDb ){
      return res.status(404).json({error: true, message: 'Email or password are incorrect!'});
    } else {
      return res.status(200).json({error: false, message: 'Sign In successfully!', token: jwtGenerate(userDb)});
    }
  } catch (error) {
    return res.status(500).json({error: true, message: 'An error was ocurred!'});
  }
}

module.exports = {
  signIn
}