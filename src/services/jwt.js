const jwt = require('jsonwebtoken');
const dotenv = require('dotenv').config();

const jwtKey = process.env.JWT_SECRET;

module.exports = jwtGenerate = (userData) => {
  const { id, name, email } = userData;
  return jwt.sign({
    id: id,
    user: name,
    email
  },
  jwtKey,
  {expiresIn: '14 days'});
}