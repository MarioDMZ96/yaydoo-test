const fs = require('fs');

const select = (filePath) => {
  try {
    return data = require(`../${filePath}`);
  } catch (error) {
    return [];
  }
}

const insert = (filePath, data) => {
  let dataToSave = JSON.stringify(data);
  fs.writeFile(`./src/${filePath}`, dataToSave, (err) => {
    if (err) console.log(err);
  });
}

module.exports = {
  select,
  insert
}